Rails.application.routes.draw do
  mount EngineMensajes::Engine => "/mensajes"
  
  root :to => "inicio#home"
  get '/grafica1',     :to => "inicio#show_partial"
  get '/grafica2',     :to => "inicio#show_graficaParroquias"
  get '/grafica3',     :to => "inicio#show_graficaMunicipios"
  get '/admaestros/',     :to => "admaestros#adm_maestros"
  get '/equipo/',     :to => "equipos#equipo"
  
end
