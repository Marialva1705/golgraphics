--
-- PostgreSQL database dump
--

-- Dumped from database version 10.0
-- Dumped by pg_dump version 10.0

-- Started on 2018-01-02 21:04:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 320 (class 1259 OID 18522)
-- Name: valoracion_categorias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE valoracion_categorias (
    id integer NOT NULL,
    tipo_perfil_id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    icono character varying(100),
    estatus character(1) DEFAULT 'A'::bpchar NOT NULL,
    fecha_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    usuario_creacion_id integer DEFAULT 0 NOT NULL,
    fecha_actualizacion timestamp without time zone,
    usuario_actualizacion_id integer
);


ALTER TABLE valoracion_categorias OWNER TO postgres;

--
-- TOC entry 3296 (class 0 OID 0)
-- Dependencies: 320
-- Name: TABLE valoracion_categorias; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE valoracion_categorias IS 'Almacena un listado de las categorias de las valoraciones con su icono';


--
-- TOC entry 321 (class 1259 OID 18528)
-- Name: valoracion_categorias_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE valoracion_categorias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE valoracion_categorias_id_seq OWNER TO postgres;

--
-- TOC entry 3297 (class 0 OID 0)
-- Dependencies: 321
-- Name: valoracion_categorias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE valoracion_categorias_id_seq OWNED BY valoracion_categorias.id;


--
-- TOC entry 3165 (class 2604 OID 18604)
-- Name: valoracion_categorias id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valoracion_categorias ALTER COLUMN id SET DEFAULT nextval('valoracion_categorias_id_seq'::regclass);


--
-- TOC entry 3290 (class 0 OID 18522)
-- Dependencies: 320
-- Data for Name: valoracion_categorias; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (1, 70, 'Juego limpio', 'ico-valor-juego-limpio.png', 'A', '2017-04-17 13:11:08.869273', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (2, 70, 'Técnica', 'ico-valor-tecnica.png', 'A', '2017-04-17 13:11:17.814961', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (3, 70, 'Velocidad', 'ico-valor-velocidad.png', 'A', '2017-04-17 13:11:26.701489', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (4, 70, 'Resistencia', 'ico-valor-resistencia.png', 'A', '2017-04-17 13:11:33.014163', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (5, 70, 'Fuerza', 'ico-valor-fuerza.png', 'A', '2017-04-17 13:11:44.606088', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (6, 76, 'Bueno', 'ico-valor-promedio.png', 'A', '2017-04-17 13:21:33.286551', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (7, 76, 'Regular', 'ico-valor-promedio.png', 'A', '2017-04-17 13:21:42.493117', 0, NULL, NULL);
INSERT INTO valoracion_categorias (id, tipo_perfil_id, nombre, icono, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (8, 76, 'Malo', 'ico-valor-promedio.png', 'A', '2017-04-17 13:21:48.350615', 0, NULL, NULL);


--
-- TOC entry 3298 (class 0 OID 0)
-- Dependencies: 321
-- Name: valoracion_categorias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('valoracion_categorias_id_seq', 8, true);


--
-- TOC entry 3167 (class 2606 OID 18747)
-- Name: valoracion_categorias pk_valoracion_categorias; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valoracion_categorias
    ADD CONSTRAINT pk_valoracion_categorias PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 19482)
-- Name: valoracion_categorias fk_valoracion_categorias_maestros; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valoracion_categorias
    ADD CONSTRAINT fk_valoracion_categorias_maestros FOREIGN KEY (tipo_perfil_id) REFERENCES adm_maestros(id);


-- Completed on 2018-01-02 21:04:13

--
-- PostgreSQL database dump complete
--

