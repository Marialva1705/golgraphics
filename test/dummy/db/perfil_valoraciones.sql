--
-- PostgreSQL database dump
--

-- Dumped from database version 10.0
-- Dumped by pg_dump version 10.0

-- Started on 2018-01-02 21:00:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 287 (class 1259 OID 18347)
-- Name: perfil_valoraciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE perfil_valoraciones (
    id integer NOT NULL,
    perfil_id integer NOT NULL,
    persona_id integer NOT NULL,
    valoracion_categoria_id integer NOT NULL,
    valoracion integer NOT NULL,
    temporada_id integer NOT NULL,
    estatus character(1) DEFAULT 'A'::bpchar NOT NULL,
    fecha_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    usuario_creacion_id integer DEFAULT 0 NOT NULL,
    fecha_actualizacion timestamp without time zone,
    usuario_actualizacion_id integer,
    comentario character varying(250)
);


ALTER TABLE perfil_valoraciones OWNER TO postgres;

--
-- TOC entry 3299 (class 0 OID 0)
-- Dependencies: 287
-- Name: TABLE perfil_valoraciones; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE perfil_valoraciones IS 'Almacena un listado de las valoraciones obtenidas por los perfiles en una temporada en particular';


--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 287
-- Name: COLUMN perfil_valoraciones.perfil_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_valoraciones.perfil_id IS 'Almacena el codigo del perfil al que se le hace la valoracion.';


--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 287
-- Name: COLUMN perfil_valoraciones.valoracion_categoria_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_valoraciones.valoracion_categoria_id IS 'Almacena el codigo de la categoria de la valoracion.';


--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 287
-- Name: COLUMN perfil_valoraciones.valoracion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_valoraciones.valoracion IS 'Almacena un numero con la valoracion, 1, 2, 3, 4, 5,';


--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 287
-- Name: COLUMN perfil_valoraciones.temporada_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_valoraciones.temporada_id IS 'Almacena el id de la temporada cuando fue guardada la información';


--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 287
-- Name: COLUMN perfil_valoraciones.comentario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_valoraciones.comentario IS 'Comentario de la Valoracion';


--
-- TOC entry 288 (class 1259 OID 18353)
-- Name: perfil_valoraciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE perfil_valoraciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE perfil_valoraciones_id_seq OWNER TO postgres;

--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 288
-- Name: perfil_valoraciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE perfil_valoraciones_id_seq OWNED BY perfil_valoraciones.id;


--
-- TOC entry 3165 (class 2604 OID 18584)
-- Name: perfil_valoraciones id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_valoraciones ALTER COLUMN id SET DEFAULT nextval('perfil_valoraciones_id_seq'::regclass);


--
-- TOC entry 3293 (class 0 OID 18347)
-- Dependencies: 287
-- Data for Name: perfil_valoraciones; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 288
-- Name: perfil_valoraciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('perfil_valoraciones_id_seq', 29, true);


--
-- TOC entry 3167 (class 2606 OID 18703)
-- Name: perfil_valoraciones pk_perfil_valoraciones; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_valoraciones
    ADD CONSTRAINT pk_perfil_valoraciones PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 19262)
-- Name: perfil_valoraciones fk_perfil_valoraciones_maestros; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_valoraciones
    ADD CONSTRAINT fk_perfil_valoraciones_maestros FOREIGN KEY (temporada_id) REFERENCES adm_maestros(id);


--
-- TOC entry 3169 (class 2606 OID 19267)
-- Name: perfil_valoraciones fk_perfil_valoraciones_perfiles; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_valoraciones
    ADD CONSTRAINT fk_perfil_valoraciones_perfiles FOREIGN KEY (perfil_id) REFERENCES perfiles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3170 (class 2606 OID 19272)
-- Name: perfil_valoraciones fk_perfil_valoraciones_personas; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_valoraciones
    ADD CONSTRAINT fk_perfil_valoraciones_personas FOREIGN KEY (persona_id) REFERENCES personas(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3171 (class 2606 OID 19277)
-- Name: perfil_valoraciones fk_perfil_valoraciones_valoracion_categorias; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_valoraciones
    ADD CONSTRAINT fk_perfil_valoraciones_valoracion_categorias FOREIGN KEY (valoracion_categoria_id) REFERENCES valoracion_categorias(id);


-- Completed on 2018-01-02 21:00:17

--
-- PostgreSQL database dump complete
--

