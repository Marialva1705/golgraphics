# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adm_maestro_tipos", force: :cascade, comment: "Almacena una lista de los tipos de listados maestros que pueden existir. Ej: \"Temporadas / Grupos / Tipos de juego\"." do |t|
    t.string "nombre", null: false, comment: "Almacena el nombre del tipo de Listado Maestro."
    t.string "descripcion", comment: "Almacena una descripcion mas detallada del tipo de Listado Maestro."
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["usuario_actualizacion_id"], name: "index_adm_maestro_tipos_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_adm_maestro_tipos_on_usuario_creacion_id"
  end

  create_table "adm_maestros", force: :cascade, comment: "Almacena una lista de los Listados Maestros, asociados a un tipo/categoria. Ej: \"Cesped Artiicial\", \"Grupo 1\", \"Futbol 11\"" do |t|
    t.bigint "adm_maestro_tipo_id", null: false, comment: "Almacena el id de la categoria/tipo a la que pertenece."
    t.string "nombre", null: false, comment: "Almacena el nombre del item del Listado Maestro."
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["adm_maestro_tipo_id"], name: "index_adm_maestros_on_adm_maestro_tipo_id"
    t.index ["usuario_actualizacion_id"], name: "index_adm_maestros_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_adm_maestros_on_usuario_creacion_id"
  end

  create_table "equipo_categorias", force: :cascade, comment: "Almacena un listado de las categorias/divisiones los equipos de futbol que realizan alguna accion dentro del portal" do |t|
    t.bigint "superior_id"
    t.bigint "federacion_id", null: false, comment: "ALmacena del ID de la federacion a la que pertenece"
    t.string "nombre", null: false
    t.integer "codigo"
    t.boolean "es_grupo", default: false, null: false, comment: "Indica si es un titulo para grupo de opciones"
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["federacion_id"], name: "index_equipo_categorias_on_federacion_id"
    t.index ["superior_id"], name: "index_equipo_categorias_on_superior_id"
    t.index ["usuario_actualizacion_id"], name: "index_equipo_categorias_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_equipo_categorias_on_usuario_creacion_id"
  end

  create_table "equipos", force: :cascade, comment: "Almacena un listado de las categorias/divisiones los equipos de futbol que realizan alguna accion dentro del portal" do |t|
    t.bigint "club_id", null: false
    t.bigint "equipo_categoria_id", null: false
    t.bigint "campo_id"
    t.string "nombre", null: false
    t.integer "codigo"
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["campo_id"], name: "index_equipos_on_campo_id"
    t.index ["club_id"], name: "index_equipos_on_club_id"
    t.index ["equipo_categoria_id"], name: "index_equipos_on_equipo_categoria_id"
    t.index ["usuario_actualizacion_id"], name: "index_equipos_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_equipos_on_usuario_creacion_id"
  end

  create_table "perfil_jugadores", force: :cascade, comment: "Almacena un listado de todos los jugadores que pertenezcan a un equipo" do |t|
    t.bigint "perfil_id", null: false, comment: "Almacena el codigo del perfil al que esta asociado el jugador."
    t.bigint "equipo_id", null: false, comment: "Almacena el codigo del equipo al que pertenece el jugador."
    t.bigint "tipo_id", null: false, comment: "Indica el tipo de Jugador: Titular o Suplente"
    t.bigint "posicion_id", null: false, array: true
    t.bigint "pierna_buena_id"
    t.bigint "temporada_id", null: false
    t.integer "numero"
    t.float "peso"
    t.float "altura"
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["equipo_id"], name: "index_perfil_jugadores_on_equipo_id"
    t.index ["perfil_id"], name: "index_perfil_jugadores_on_perfil_id"
    t.index ["pierna_buena_id"], name: "index_perfil_jugadores_on_pierna_buena_id"
    t.index ["posicion_id"], name: "index_perfil_jugadores_on_posicion_id"
    t.index ["temporada_id"], name: "index_perfil_jugadores_on_temporada_id"
    t.index ["tipo_id"], name: "index_perfil_jugadores_on_tipo_id"
    t.index ["usuario_actualizacion_id"], name: "index_perfil_jugadores_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_perfil_jugadores_on_usuario_creacion_id"
  end

  create_table "perfil_valoraciones", id: :serial, force: :cascade, comment: "Almacena un listado de las valoraciones obtenidas por los perfiles en una temporada en particular" do |t|
    t.integer "perfil_id", null: false, comment: "Almacena el codigo del perfil al que se le hace la valoracion."
    t.integer "persona_id", null: false
    t.integer "valoracion_categoria_id", null: false, comment: "Almacena el codigo de la categoria de la valoracion."
    t.integer "valoracion", null: false, comment: "Almacena un numero con la valoracion, 1, 2, 3, 4, 5,"
    t.integer "temporada_id", null: false, comment: "Almacena el id de la temporada cuando fue guardada la información"
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.integer "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.integer "usuario_actualizacion_id"
    t.string "comentario", limit: 250, comment: "Comentario de la Valoracion"
  end

  create_table "perfiles", force: :cascade, comment: "Almacena un listado de las datos basicos de las perfiles que realizan alguna accion dentro del portal" do |t|
    t.bigint "persona_id", null: false
    t.bigint "tipo_id", null: false, comment: "Indica el tipo de perfil: Jugador / Directivo / Aficionado / Entrenador"
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["persona_id"], name: "index_perfiles_on_persona_id"
    t.index ["tipo_id"], name: "index_perfiles_on_tipo_id"
    t.index ["usuario_actualizacion_id"], name: "index_perfiles_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_perfiles_on_usuario_creacion_id"
  end

  create_table "personas", force: :cascade, comment: "Almacena un listado de las datos basicos de las personas que realizan alguna accion dentro del portal" do |t|
    t.bigint "sexo_id", null: false
    t.bigint "adm_usuario_id", null: false, comment: "Almacena el codigo del usuario que esta registrando los datos."
    t.bigint "territorio_nivel1_id", null: false
    t.bigint "territorio_nivel2_id", null: false
    t.bigint "territorio_nivel3_id"
    t.bigint "territorio_nivel4_id"
    t.bigint "nacionalidad_id"
    t.bigint "nacionalidad2_id"
    t.bigint "nivel_estudios_id"
    t.string "primer_nombre", null: false
    t.string "segundo_nombre"
    t.string "primer_apellido", null: false
    t.string "segundo_apellido"
    t.string "apodo"
    t.date "fecha_nacimiento", null: false
    t.string "biografia"
    t.string "telefono"
    t.string "foto"
    t.string "direccion"
    t.string "codigo_postal"
    t.string "movil"
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.bigint "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.bigint "usuario_actualizacion_id"
    t.index ["adm_usuario_id"], name: "index_personas_on_adm_usuario_id"
    t.index ["nacionalidad2_id"], name: "index_personas_on_nacionalidad2_id"
    t.index ["nacionalidad_id"], name: "index_personas_on_nacionalidad_id"
    t.index ["nivel_estudios_id"], name: "index_personas_on_nivel_estudios_id"
    t.index ["sexo_id"], name: "index_personas_on_sexo_id"
    t.index ["territorio_nivel1_id"], name: "index_personas_on_territorio_nivel1_id"
    t.index ["territorio_nivel2_id"], name: "index_personas_on_territorio_nivel2_id"
    t.index ["territorio_nivel3_id"], name: "index_personas_on_territorio_nivel3_id"
    t.index ["territorio_nivel4_id"], name: "index_personas_on_territorio_nivel4_id"
    t.index ["usuario_actualizacion_id"], name: "index_personas_on_usuario_actualizacion_id"
    t.index ["usuario_creacion_id"], name: "index_personas_on_usuario_creacion_id"
  end

  create_table "valoracion_categorias", id: :serial, force: :cascade, comment: "Almacena un listado de las categorias de las valoraciones con su icono" do |t|
    t.integer "tipo_perfil_id", null: false
    t.string "nombre", limit: 50, null: false
    t.string "icono", limit: 100
    t.string "estatus", limit: 1, default: "A", null: false
    t.datetime "fecha_creacion", null: false
    t.integer "usuario_creacion_id", default: 0, null: false
    t.datetime "fecha_actualizacion"
    t.integer "usuario_actualizacion_id"
  end

  add_foreign_key "adm_maestros", "adm_maestro_tipos"
  add_foreign_key "perfil_jugadores", "adm_maestros", column: "pierna_buena_id"
  add_foreign_key "perfil_jugadores", "adm_maestros", column: "temporada_id"
  add_foreign_key "perfil_jugadores", "adm_maestros", column: "tipo_id"
  add_foreign_key "perfil_jugadores", "equipos"
  add_foreign_key "perfil_jugadores", "perfiles", column: "perfil_id"
  add_foreign_key "perfil_valoraciones", "adm_maestros", column: "temporada_id", name: "fk_perfil_valoraciones_maestros"
  add_foreign_key "perfil_valoraciones", "perfiles", column: "perfil_id", name: "fk_perfil_valoraciones_perfiles", on_update: :cascade, on_delete: :cascade
  add_foreign_key "perfil_valoraciones", "personas", name: "fk_perfil_valoraciones_personas", on_update: :cascade, on_delete: :cascade
  add_foreign_key "perfil_valoraciones", "valoracion_categorias", name: "fk_perfil_valoraciones_valoracion_categorias"
  add_foreign_key "perfiles", "adm_maestros", column: "tipo_id"
  add_foreign_key "perfiles", "personas"
  add_foreign_key "personas", "adm_maestros", column: "nivel_estudios_id"
  add_foreign_key "personas", "adm_maestros", column: "sexo_id"
  add_foreign_key "valoracion_categorias", "adm_maestros", column: "tipo_perfil_id", name: "fk_valoracion_categorias_maestros"
end
