--
-- PostgreSQL database dump
--

-- Dumped from database version 10.0
-- Dumped by pg_dump version 10.0

-- Started on 2018-01-02 20:59:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 279 (class 1259 OID 18308)
-- Name: perfil_jugadores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE perfil_jugadores (
    id integer NOT NULL,
    perfil_id integer NOT NULL,
    equipo_id integer NOT NULL,
    tipo_id integer NOT NULL,
    numero integer,
    posicion_id integer[] NOT NULL,
    peso double precision,
    altura double precision,
    pierna_buena_id integer,
    temporada_id integer NOT NULL,
    estatus character(1) DEFAULT 'A'::bpchar NOT NULL,
    fecha_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    usuario_creacion_id integer DEFAULT 0 NOT NULL,
    fecha_actualizacion timestamp without time zone,
    usuario_actualizacion_id integer
);


ALTER TABLE perfil_jugadores OWNER TO postgres;

--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 279
-- Name: TABLE perfil_jugadores; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE perfil_jugadores IS 'Almacena un listado de todos los jugadores que pertenescan a un equipo';


--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 279
-- Name: COLUMN perfil_jugadores.perfil_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_jugadores.perfil_id IS 'Almacena el codigo del perfil al que esta asociado el jugador.';


--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 279
-- Name: COLUMN perfil_jugadores.equipo_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_jugadores.equipo_id IS 'Almacena el codigo del equipo al que pertenece el jugador.';


--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 279
-- Name: COLUMN perfil_jugadores.tipo_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_jugadores.tipo_id IS 'Indica el tipo de Jugador: Titular o Suplente';


--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 279
-- Name: COLUMN perfil_jugadores.temporada_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN perfil_jugadores.temporada_id IS 'Almacena el id de la temporada en curso';


--
-- TOC entry 280 (class 1259 OID 18317)
-- Name: perfil_jugadores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE perfil_jugadores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE perfil_jugadores_id_seq OWNER TO postgres;

--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 280
-- Name: perfil_jugadores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE perfil_jugadores_id_seq OWNED BY perfil_jugadores.id;


--
-- TOC entry 3165 (class 2604 OID 18580)
-- Name: perfil_jugadores id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores ALTER COLUMN id SET DEFAULT nextval('perfil_jugadores_id_seq'::regclass);


--
-- TOC entry 3294 (class 0 OID 18308)
-- Dependencies: 279
-- Data for Name: perfil_jugadores; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil_jugadores (id, perfil_id, equipo_id, tipo_id, numero, posicion_id, peso, altura, pierna_buena_id, temporada_id, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (11, 42, 15, 37, 4, '{NULL,22}', 85, 182, 13, 47, 'A', '2017-08-24 13:25:41.894457', 67, NULL, NULL);
INSERT INTO perfil_jugadores (id, perfil_id, equipo_id, tipo_id, numero, posicion_id, peso, altura, pierna_buena_id, temporada_id, estatus, fecha_creacion, usuario_creacion_id, fecha_actualizacion, usuario_actualizacion_id) VALUES (12, 44, 1, 37, 6, '{NULL,23}', 100, 187, 13, 47, 'A', '2017-08-25 12:28:33.809746', 72, NULL, NULL);


--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 280
-- Name: perfil_jugadores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('perfil_jugadores_id_seq', 12, true);


--
-- TOC entry 3167 (class 2606 OID 18695)
-- Name: perfil_jugadores pk_perfil_jugadores; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores
    ADD CONSTRAINT pk_perfil_jugadores PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 19202)
-- Name: perfil_jugadores fk_perfil_jugadores_equipos; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores
    ADD CONSTRAINT fk_perfil_jugadores_equipos FOREIGN KEY (equipo_id) REFERENCES equipos(id);


--
-- TOC entry 3169 (class 2606 OID 19207)
-- Name: perfil_jugadores fk_perfil_jugadores_maestros; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores
    ADD CONSTRAINT fk_perfil_jugadores_maestros FOREIGN KEY (tipo_id) REFERENCES adm_maestros(id);


--
-- TOC entry 3170 (class 2606 OID 19212)
-- Name: perfil_jugadores fk_perfil_jugadores_maestros2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores
    ADD CONSTRAINT fk_perfil_jugadores_maestros2 FOREIGN KEY (pierna_buena_id) REFERENCES adm_maestros(id);


--
-- TOC entry 3171 (class 2606 OID 19217)
-- Name: perfil_jugadores fk_perfil_jugadores_maestros3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores
    ADD CONSTRAINT fk_perfil_jugadores_maestros3 FOREIGN KEY (temporada_id) REFERENCES adm_maestros(id);


--
-- TOC entry 3172 (class 2606 OID 19222)
-- Name: perfil_jugadores fk_perfil_jugadores_perfiles; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_jugadores
    ADD CONSTRAINT fk_perfil_jugadores_perfiles FOREIGN KEY (perfil_id) REFERENCES perfiles(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-01-02 20:59:21

--
-- PostgreSQL database dump complete
--

