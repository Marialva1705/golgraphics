# encoding: utf-8
class AdmTerritorio < ActiveRecord::Base
  self.table_name = 'adm_territorios'

  belongs_to :adm_territorio, foreign_key: :nivel_superior_id # modelo - en singular
  has_many :personas 

  # --------------  TITULOS DE LOS FORMULARIOS ---------------------------------------------
  TITULOS = {
    index: 'Listado de Territorios existentes',
    show: 'Consulta de Territorios existentes',
    new: 'Ingresar Territorio nuevo',
    edit: 'Editar Territorio existente'
  }

  # --------------  CAMPOS A MOSTRAR EN LOS FORMULARIOS INDEX Y SHOW -----------------------
  # Especifique lo atributos que desee aparezcan en las vistas.
  # El orden es importante. Por ej., en la vista index, se mostrará de 1ro el campo ID, seguido de DESCRIPCION, etc.
  VISTAS = [
    { nombre: [ :index, :show ] },
    { abreviatura: [ :index, :show ] },
    { nivel_superior: [ :index, :show ] },
    { nivel: [ :index, :show ] },
    { estado: [ :index, :show ] },
    { creado: [ :show ] },
    { actualizado: [ :show ] },
  ]

  # --------------  CAMPOS A SER MODIFICADOS POR EL USUARIO ------------------------------
  PARAMETROS = :nombre, :abreviatura, :imagen, :nivel_superior_id, :nivel, :estatus;

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :nombre,
    presence: true,
    length: { within: 2..100 },
    format: { with: /\A[A-Za-zÑÁÉÍÓÚÜñ][A-Za-z.ÑÁÉÍÓÚÜñ ]+\z/,
              message: '* Solo se permiten letras'
  }

  validates :estatus,
    inclusion: { in: ['A', 'N', 'B']
  }

  # ---------------- LISTADO DE TERRIROTIOS PARA EL SELECT ---------------------
  def self.buscar_todos_activos_por_nivel(nivel)
    select('id, nombre, abreviatura, nivel_superior_id').
      where("estatus = 'A' AND nivel = #{nivel}").
      order('nombre')
  end

  # ---------------- LISTADO DE TERRIROTIOS PARA EL SELECT POR ID SUPERIOR---------------------
  def self.buscar_todos_activos_por_id_superior(id_superior)
    select('id, nombre, abreviatura, nivel_superior_id').
      where("estatus = 'A' AND nivel_superior_id = #{id_superior}").
      order('nombre')
  end

  # ---------------- LISTADO DE NACIONALIDADES PARA EL SELECT ---------------------
  def self.buscar_todas_nacionalidaes_activas
    select('id, gentilicio').
      where("estatus = 'A' AND nivel = 1").
      order('nombre')
  end

   def most_purchased
    AdmTerritorio.group(:id).order(nombre: :desc).limit(10).pluck("nombre, count(*) as quantity_sum")
    #.group(:nombre).order(nombre: :desc).last(10).pluck(:nombre)
    #.pluck("nombre, count(*) as quantity_sum")
   end 

  #
  #     # ---------------- BUSQUEDA UTILIZADA EN EL SHOW ---------------------
  #     def self.mostrar_busqueda(id)
  #          select("adm_provincias.*, adm_paises.nombre AS pais,
  #                        (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.usuario_id WHERE u.id = adm_provincias.usuario_creacion_id) || ' - ' || to_char(adm_provincias.fecha_creacion, 'DD/MM/YYYY HH:MI AM') AS creado,
  #                        (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.usuario_id WHERE u.id = adm_provincias.usuario_actualizacion_id) || ' - ' || to_char(adm_provincias.fecha_actualizacion, 'DD/MM/YYYY HH:MI AM') AS actualizado#").
  #               joins(:pais).
  #               where("adm_provincias.pais_id = paises.id AND adm_provincias.estatus <> 'B' AND adm_provincias.id = #{id}").
  #               first
  #     end
end
