# encoding: utf-8

class AdmMaestro < ActiveRecord::Base
  self.table_name = 'adm_maestros'

  belongs_to :adm_maestro_tipo, foreign_key: :adm_maestro_tipo_id # modelo - en singular

  # --------------  TITULOS DE LOS FORMULARIOS ---------------------------------------------
  TITULOS = {
    index: 'Listado de Tipos de Listados Maestros existentes',
    show: 'Consulta de Tipo de Listado Maestro existente',
    new: 'Ingresar Tipo de Listado Maestro nuevo',
    edit: 'Editar Tipo de Listado Maestro existente'
  }

  # --------------  CAMPOS A MOSTRAR EN LOS FORMULARIOS INDEX Y SHOW -----------------------
  # Especifique lo atributos que desee aparezcan en las vistas.
  # El orden es importante. Por ej., en la vista index, se mostrará de 1ro el campo ID, seguido de DESCRIPCION, etc.
  VISTAS = [
    { tipo_maestro: [ :index, :show ] },
    { nombre: [ :index, :show ] },
    { estado: [ :index, :show ] },
    { creado: [ :show ] },
    { actualizado: [ :show ] },
  ]

  # --------------  CAMPOS A SER MODIFICADOS POR EL USUARIO ------------------------------
  PARAMETROS = :nombre, :adm_maestro_tipo_id, :estatus;

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :nombre,
    presence: true,
    length: { within: 2..99 }

  validates :adm_maestro_tipo_id,
    presence: true

  validates :estatus,
    inclusion: { in: ['A', 'N', 'B']
  }

  #---------------------------------------------------------------------------------
  # --------------- METODOS DE LA CLASE --------------------------------------------
  #
  #     # ---------------- BUSQUEDA UTILIZADA EN EL INDEX ---------------------
  #     def self.buscar_todos(condicion)
  #          select("adm_maestros.id, adm_maestros.nombre, adm_maestros.adm_maestro_tipo_id, adm_maestro_tipos.nombre AS tipo_maestro, adm_maestros.estatus")
  #               .joins(:adm_maestro_tipo)
  #               .where("adm_maestros.estatus <> 'B' #{condicion}")
  #     end
  #
  #
  #     # ---------------- BUSQUEDA UTILIZADA EN EL SHOW ---------------------
  #     def self.buscar_por(id)
  #          select("adm_maestros.*, adm_maestros.nombre, adm_maestros.adm_maestro_tipo_id, adm_maestro_tipos.nombre AS tipo_maestro
  #                  (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.usuario_id WHERE u.id = adm_maestros.usuario_creacion_id) || ' - ' || to_char(adm_maestros.fecha_creacion, 'DD/MM/YYYY HH:MI AM') AS creado,
  #                  (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.usuario_id WHERE u.id = adm_maestros.usuario_actualizacion_id) || ' - ' || to_char(adm_maestros.fecha_actualizacion, 'DD/MM/YYYY HH:MI AM') AS actualizado#")
  #               .joins(:adm_maestro_tipo)
  #               .where("adm_maestros.estatus <> 'B' AND adm_maestros.id = #{id}")
  #               .first
  #     end

  # --------------------------------------------------------------------
  def self.buscar_todos_por_tipo(nombre, condicion)
    select('adm_maestros.id, adm_maestros.nombre, adm_maestros.adm_maestro_tipo_id, adm_maestro_tipos.nombre AS tipo_maestro')
      .joins(:adm_maestro_tipo)
      .where("adm_maestro_tipos.nombre = '#{nombre}' #{condicion}")
  end

  def self.buscar_temporada_actual
    select('adm_maestros.id, adm_maestros.nombre')
      .joins(:adm_maestro_tipo)
      .where("adm_maestro_tipos.nombre = 'Temporadas' AND adm_maestros.estatus = 'A'")
      .order('id')
      .last
  end
  def buscar_maestro_tipos
    AdmMaestro.joins(:adm_maestro_tipo).where("adm_maestros.estatus = 'A'").group("adm_maestro_tipos.id").pluck("adm_maestro_tipos.nombre, count(*) as total")
  end
end
