# encoding: utf-8

class Persona < ActiveRecord::Base
  #include ModeloMetodosGenerales
  self.table_name = 'personas'

  # before_destroy :eliminar_amistades, prepend: true
  # before_destroy :eliminar_novedades, prepend: true
  
  # --------------  RELACIONES CON OTRAS TABLAS ------------------------------
  # has_many   :persona_amigos,     dependent: :destroy
   belongs_to :adm_usuario,        foreign_key: :adm_usuario_id
   belongs_to :adm_territorio,     foreign_key: :territorio_nivel1_id
  # has_many   :perfiles,           dependent: :destroy
  # has_many   :perfil_comentarios, dependent: :destroy
  # has_many   :perfil_fotos,       through: :perfiles
  # has_many   :perfil_videos,      through: :perfiles

  # accepts_nested_attributes_for :perfiles,
  #   reject_if: proc { |attr| attr['tipo_id'].blank? },
  #   allow_destroy: true

  #mount_uploader :foto, ImagenPersonaUploader

  # -------  CAMPOS A SER MODIFICADOS POR EL USUARIO ----------------------
  PARAMETROS = :primer_nombre,
    :segundo_nombre, :primer_apellido,
    :segundo_apellido, :apodo, :sexo_id,
    :fecha_nacimiento, :adm_usuario_id,
    :biografia, :territorio_nivel1_id,
    :territorio_nivel2_id, :territorio_nivel3_id,
    :territorio_nivel4_id, :direccion, :codigo_postal,
    :telefono, :movil, :peso, :pais_id, :altura,
    :nacionalidad_id, :nacionalidad2_id, :nivel_estudios_id, :foto, :estatus; #,
    # { perfiles_attributes: Perfil::PARAMETROS };

  # --------------- VALIDACIONES DE LOS CAMPOS ----------------------------
  validates :primer_nombre, :primer_apellido,
    presence:   true,
    length:     { within: 1..50 }

  validates :segundo_nombre, :segundo_apellido,
    length:     { maximum: 50 }

  validates :adm_usuario_id,
    :fecha_nacimiento,
    :territorio_nivel1_id,
    :territorio_nivel2_id,
    :territorio_nivel3_id,
    :nacionalidad_id,
    presence:   true

  validates :estatus,
    inclusion:  { in: ['A', 'N', 'B']
  }

  # -----------------------------------------------------------------
  # ---- METODOS DE LA CLASE ----------------------------------------

  # ------------------------------------------------------------------------------
  # def obtener_foto
  #   if self.foto.blank?
  #     'silueta_persona-g-gris.png'
  #   else
  #     self.foto
  #   end
  # end

  # ------------------------------------------------------------------------------
  # def nombre_persona
  #   "#{self[:primer_nombre]} #{self[:primer_apellido]}"
  # end
  
  # ------------------------------------------------------------------------------
  # def self.buscar_por_varios(txt)
  #   select("*,
  #           (SELECT id FROM perfiles WHERE persona_id = personas.id ORDER BY fecha_creacion DESC LIMIT 1) AS perfil_id").
  #     where("estatus = 'A' AND UPPER(primer_nombre) LIKE UPPER('%#{txt}%')
  #                       OR UPPER(segundo_nombre) LIKE UPPER('%#{txt}%')
  #                       OR UPPER(primer_apellido) LIKE UPPER('%#{txt}%')
  #                       OR UPPER(segundo_apellido) LIKE UPPER('%#{txt}%')
  #                       OR UPPER(apodo) LIKE UPPER('%#{txt}%')")
  # end

  #---------------------------------------------------------------------------------
  # def self.buscar_por_usuario(id)
  #   select("*,  primer_nombre || ' ' || primer_apellido AS nombre_completo,
  #                   (SELECT m.nombre FROM adm_maestros AS m WHERE m.id = personas.sexo_id) AS sexo,
  #                   (SELECT gentilicio FROM adm_territorios WHERE id = personas.nacionalidad_id) AS nacionalidad1,
  #                   (SELECT nombre FROM adm_territorios WHERE id = personas.territorio_nivel1_id) AS territorio_nivel1,
  #                   (SELECT nombre FROM adm_territorios WHERE id = personas.territorio_nivel2_id) AS territorio_nivel2,
  #                   (SELECT nombre FROM adm_territorios WHERE id = personas.territorio_nivel3_id) AS territorio_nivel3,
  #                   (SELECT nombre FROM adm_territorios WHERE id = personas.territorio_nivel4_id) AS territorio_nivel4,
  #                   (SELECT array_agg(pa.amigo_id) FROM persona_amigos AS pa WHERE pa.persona_id = personas.id ) AS amigos,
  #                   (SELECT array_agg(pe.id || '-' || pe.tipo_id || '-' || m.nombre) FROM perfiles pe, adm_maestros m WHERE pe.persona_id = personas.id AND m.id = pe.tipo_id) AS arr_perfiles,
  #                   (SELECT array_agg(pe.id) FROM perfiles pe WHERE pe.persona_id = personas.id) AS perfiles_ids,
  #                   (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = personas.nivel_estudios_id) AS nivel_estudios")
  #   .where("estatus <> 'B' AND adm_usuario_id = #{id}")
  #   .first
  # end

  #-------------------------------------------------------------------------
  #def self.buscar_sugerencias(cantidad,
    #   contactos_actuales,
    #   id_persona_actual,
    #   id_equipo, tipos_perfil,
    #   id_pais,
    #   id_comunidad,
    #   id_provincia)
    # campos = "m.nombre AS tipo_perfil,
    #           pf.id AS perfil_id,
    #           pe.id AS persona_id,
    #           pe.primer_nombre,
    #           pe.primer_apellido, pe.apodo, pe.foto,
    #           (SELECT t2.nombre
    #             FROM adm_territorios AS t2
    #             WHERE t2.id = pe.territorio_nivel2_id) AS territorio2,
    #               (SELECT t3.nombre
    #                 FROM adm_territorios AS t3
    #                 WHERE t3.id = pe.territorio_nivel3_id) AS territorio3,
    #             to_char(pe.fecha_nacimiento, 'DD/MM/YYYY') AS fecha_nacimiento2"
    # if tipos_perfil == 'Entrenador/a' || tipos_perfil == 'Aficionado/a' || tipos_perfil == 'Preparador/a Físico' || tipos_perfil == 'Entrenador/a Porteros/as' || tipos_perfil == 'Delegado/a' || tipos_perfil == 'Analista'

    #   id_equipo = id_equipo.to_s.gsub('[', '{').gsub(']', '}').gsub('nil', 'null')

    #   id_equipo = '{null}' if id_equipo == 'null'

    #   filtro_equipos = "(pj.equipo_id = ANY('#{id_equipo}')
    #                     OR pd.club_id = ANY( SELECT club_id FROM equipos e WHERE e.id = ANY('#{id_equipo}'))
    #                       OR '#{id_equipo}' && pa.equipo_id
    #                       OR '#{id_equipo}' && pet.equipo_id
    #                       OR '#{id_equipo}' && pp.equipo_id
    #                       OR '#{id_equipo}' && ppe.equipo_id
    #                       OR '#{id_equipo}' && pde.equipo_id
    #                       OR '#{id_equipo}' && pana.equipo_id)"
    # else
    #   filtro_equipos = "(pj.equipo_id = #{id_equipo}
    #                     OR pd.club_id = ( SELECT club_id FROM equipos e WHERE e.id = #{id_equipo})
    #                         OR #{id_equipo} = ANY(pa.equipo_id)
    #                         OR #{id_equipo} = ANY(pet.equipo_id)
    #                         OR #{id_equipo} = ANY(pp.equipo_id)
    #                         OR #{id_equipo} = ANY(ppe.equipo_id)
    #                         OR #{id_equipo} = ANY(pde.equipo_id)
    #                         OR #{id_equipo} = ANY(pana.equipo_id))"
    # end
    # sql = "-- MISMA ZONA GEOGRAFICA ---
    #             SELECT DISTINCT #{campos}
    #             FROM perfiles pf
    #             INNER JOIN personas pe ON pe.id = pf.persona_id
    #             INNER JOIN adm_maestros m ON m.id = pf.tipo_id
    #             WHERE pe.territorio_nivel1_id = #{id_pais}
    #                 AND pe.territorio_nivel2_id = #{id_comunidad}
    #                 AND pe.territorio_nivel3_id = #{id_provincia}
    #                 AND NOT pe.id = ANY ('#{contactos_actuales}')
    #             UNION
    #             -- MISMO PERFIL ----
    #             SELECT DISTINCT #{campos}
    #             FROM perfiles pf
    #             INNER JOIN personas pe ON pe.id = pf.persona_id
    #             INNER JOIN adm_maestros m ON m.id = pf.tipo_id
    #             WHERE m.nombre = '#{tipos_perfil}'
    #                 AND NOT pe.id = ANY ('#{contactos_actuales}')
    #             UNION
    #             -- MISMO EQUIPO ----
    #             SELECT DISTINCT #{campos}
    #               FROM perfiles pf
    #               INNER JOIN personas pe ON pe.id = pf.persona_id
    #             INNER JOIN adm_maestros m ON m.id = pf.tipo_id
    #             LEFT JOIN perfil_jugadores pj ON pj.perfil_id = pf.id
    #             LEFT JOIN perfil_directivos pd ON pd.perfil_id = pf.id
    #             LEFT JOIN perfil_aficionados pa ON pa.perfil_id = pf.id
    #             LEFT JOIN perfil_entrenadores pet ON pet.perfil_id = pf.id
    #             LEFT JOIN perfil_fisico_preparadores pp ON pp.perfil_id = pf.id
    #             LEFT JOIN perfil_portero_entrenadores ppe ON ppe.perfil_id = pf.id
    #             LEFT JOIN perfil_delegados pde ON pde.perfil_id = pf.id
    #             LEFT JOIN perfil_analistas pana ON pana.perfil_id = pf.id
    #             WHERE #{filtro_equipos}
    #                 AND NOT pe.id = ANY ('#{contactos_actuales}')

    #             UNION

    #             -- AMIGOS DE MIS AMIGOS
    #             SELECT DISTINCT #{campos}
    #             FROM perfiles pf
    #             INNER JOIN personas pe ON pe.id = pf.persona_id
    #             INNER JOIN adm_maestros m ON m.id = pf.tipo_id
    #             INNER JOIN persona_amigos pa ON pa.amigo_id = pe.id
    #             WHERE pa.persona_id = ANY(SELECT pe.id FROM personas pe INNER JOIN persona_amigos pa ON pa.amigo_id = pe.id WHERE pa.persona_id = #{id_persona_actual})
    #                 AND
    #                 NOT pe.id = ANY ('#{contactos_actuales}')
    #                 LIMIT #{cantidad}"

    # find_by_sql(sql)
  #end

  #-----------------------------------------------------------------------
  def self.buscar_amigos_por_persona(id)
    select('*')
    .where("estatus <> 'B' AND id = #{id}")
    .first
  end

  #---------------------------------------------------------------------
  def self.buscar_invitaciones_enviadas(id)
    #         select("personas.*")
    #         .joins(:persona_amigos)
    #         .where("persona_amigos.persona_id = #{id} AND persona_amigos.estatus = 'S'")

    # sql = "SELECT personas.*, persona_amigos.id AS solicitud_id, persona_amigos.amigo_id,
    #             (SELECT id FROM perfiles WHERE persona_id = personas.id ORDER BY id DESC LIMIT 1) AS perfil_id
    #             FROM personas
    #             INNER JOIN persona_amigos ON persona_amigos.amigo_id = personas.id
    #             WHERE (persona_amigos.persona_id = #{id} AND persona_amigos.estatus = 'S')"

    # find_by_sql(sql)
  end

  #-------------------------------------------------------------------
  # def self.buscar_invitaciones_recibidas(id)
  #   sql = "SELECT personas.*, persona_amigos.id AS solicitud_id, persona_amigos.amigo_id,
  #               (SELECT id FROM perfiles WHERE persona_id = personas.id ORDER BY id DESC LIMIT 1) AS perfil_id
  #               FROM personas
  #               INNER JOIN persona_amigos ON persona_amigos.persona_id = personas.id
  #               WHERE (persona_amigos.amigo_id = #{id} AND persona_amigos.estatus = 'S')"
  #   find_by_sql(sql)
  # end

  #----------------------------------------------------------------------
  #def self.buscar_amigos_equipo_rivales_por_persona(id_persona, id_equipo, id_temporada)
    # sql = " SELECT DISTINCT p.id, primer_nombre || ' ' || primer_apellido AS nombre, foto
    #             FROM personas p, persona_amigos pa
    #             WHERE p.id = pa.amigo_id
    #                 AND pa.persona_id = #{id_persona}

    #             UNION

    #             SELECT p.id, primer_nombre || ' ' || primer_apellido AS nombre, foto
    #             FROM personas p
    #             INNER JOIN perfiles pf ON pf.persona_id = p.id
    #             LEFT JOIN perfil_jugadores pj ON pj.perfil_id = pf.id
    #             LEFT JOIN perfil_aficionados pa ON pa.perfil_id = pf.id
    #             LEFT JOIN perfil_entrenadores pe ON pe.perfil_id = pf.id
    #             LEFT JOIN perfil_fisico_preparadores pp ON pp.perfil_id = pf.id
    #             LEFT JOIN perfil_portero_entrenadores ppe ON ppe.perfil_id = pf.id
    #             LEFT JOIN perfil_delegados pd ON pd.perfil_id = pf.id
    #             WHERE ((pj.equipo_id = #{id_equipo} AND pj.temporada_id = #{id_temporada})
    #                 OR (#{id_equipo} = ANY(pa.equipo_id) AND pa.temporada_id = #{id_temporada})
    #                 OR (#{id_equipo} = ANY(pe.equipo_id) AND pe.temporada_id = #{id_temporada}))"
    ## *************** FALTAN LOS RIVALES ---------------
    #find_by_sql(sql)
  #end

  #----------------------------------------------------------------------
  # def self.buscar_etiquetados_en_publicaciones_por_perfil(id_perfil)
  #   campos = "SELECT DISTINCT p.id, p.primer_nombre, p.primer_apellido, p.foto,
  #               (SELECT id FROM perfiles WHERE persona_id = p.id ORDER BY id DESC LIMIT 1) AS perfil_etiquetado_id
  #               FROM personas p, perfil_comentarios pc
  #               WHERE p.id = ANY (pc.etiquetar_a)"

  #   sql = campos +
  #     " AND perfil_id = #{id_perfil}" +
  #     ' UNION ' +
  #     campos +
  #     ' AND persona_id = 2'
  #   find_by_sql(sql)
  # end

  #-------------------------------------------------------------------------
  # def self.buscar_etiquetados_en_fotos_por_persona(id_persona)
  #   sql = "SELECT DISTINCT p.id, p.primer_nombre, p.primer_apellido, p.foto,
  #               (SELECT id FROM perfiles WHERE persona_id = p.id ORDER BY id DESC LIMIT 1) AS perfil_etiquetado_id
  #               FROM personas p, perfil_fotos pf
  #               INNER JOIN perfiles pe ON pe.id = pf.perfil_id
  #               WHERE p.id = ANY (pf.etiquetar_a)
  #                   AND pe.persona_id = #{id_persona}"
  #   find_by_sql(sql)
  # end

  #-------------------------------------------------------------------------
  # def self.buscar_etiquetados_en_videos_por_persona(id_persona)
  #   sql = "SELECT DISTINCT p.id, p.primer_nombre, p.primer_apellido, p.foto,
  #               (SELECT id FROM perfiles WHERE persona_id = p.id ORDER BY id DESC LIMIT 1) AS perfil_etiquetado_id
  #               FROM personas p, perfil_videos pv
  #               INNER JOIN perfiles pe ON pe.id = pv.perfil_id
  #               WHERE p.id = ANY (pv.etiquetar_a)
  #                   AND pe.persona_id = #{id_persona}"
  #   find_by_sql(sql)
  # end

  # def comentarios_con_fotos
  #   perfil_comentarios.where.not('archivo' => nil)
  # end

  # def comentarios_con_videos
  #   perfil_comentarios.where.not('video_url' => '')
  # end

  # private
  
  #   def eliminar_amistades
  #     PersonaAmigo.where(persona_id: self.id).destroy_all
  #     PersonaAmigo.where(amigo_id: self.id).destroy_all
  #   end

  #   def eliminar_novedades
  #     PersonaNovedad.where(persona_origen_id: self.id).destroy_all
  #     PersonaNovedad.where(persona_destino_id: self.id).destroy_all
  #   end
  def mostrar_usuarios_por_paises
    Persona.joins(:adm_territorio,:adm_usuario).where("adm_usuarios.estatus = 'A'").group("adm_territorios.id").pluck("adm_territorios.nombre, count(*) as total")
  end
  def mostrar_usuarios_por_paises_id
    Persona.joins(:adm_territorio,:adm_usuario).where("adm_usuarios.estatus = 'A'").group("adm_territorios.id").pluck("adm_territorios.id,adm_territorios.nombre, count(*) as total")
  end
  def mostrar_usuarios_por_ciudad(id)
    sql = "SELECT COUNT(*) AS total, adt.nombre,p.territorio_nivel2_id FROM personas p
     INNER JOIN adm_usuarios au ON p.adm_usuario_id=au.id 
     INNER JOIN adm_territorios adt ON adt.id=p.territorio_nivel2_id 
     AND adt.estatus='A' WHERE au.estatus = 'A' AND p.territorio_nivel1_id= #{id} group by adt.nombre,p.territorio_nivel2_id"
     Persona.find_by_sql(sql)
  end
  def mostrar_usuarios_por_parroquia(idPais,idCiudad)
    sql = "SELECT COUNT(*) AS total, adt.nombre,p.territorio_nivel3_id FROM personas p 
    INNER JOIN adm_usuarios au ON p.adm_usuario_id=au.id 
    INNER JOIN adm_territorios adt ON adt.id=p.territorio_nivel3_id AND adt.estatus='A'
    WHERE au.estatus = 'A' AND p.territorio_nivel1_id= #{idPais} AND p.territorio_nivel2_id=#{idCiudad} group by adt.nombre,p.territorio_nivel3_id"
     Persona.find_by_sql(sql)
  end
  def mostrar_usuarios_por_municipio(idPais,idCiudad,idMunicipio)
    sql = "SELECT COUNT(*) AS total, adt.nombre  FROM personas p 
    INNER JOIN adm_usuarios au ON p.adm_usuario_id=au.id 
    INNER JOIN adm_territorios adt ON adt.id=p.territorio_nivel4_id AND adt.estatus='A' 
    WHERE au.estatus = 'A' AND p.territorio_nivel1_id=#{idPais} AND p.territorio_nivel2_id=#{idCiudad} 
    AND p.territorio_nivel3_id=#{idMunicipio} group by adt.nombre"
     Persona.find_by_sql(sql)
  end
end
