# encoding: utf-8

class AdmMaestroTipo < ActiveRecord::Base
  #include ModeloMetodosGenerales
  self.table_name = 'adm_maestro_tipos'
  has_many :adm_maestros # tabla - en plural

  # --------------  TITULOS DE LOS FORMULARIOS ---------------------------------------------
  TITULOS = {
    index: 'Listado de Tipos de Listados Maestros existentes',
    show: 'Consulta de Tipo de Listado Maestro existente',
    new: 'Ingresar Tipo de Listado Maestro nuevo',
    edit: 'Editar Tipo de Listado Maestro existente'
  }

  # --------------  CAMPOS A MOSTRAR EN LOS FORMULARIOS INDEX Y SHOW -----------------------
  # Especifique lo atributos que desee aparezcan en las vistas.
  # El orden es importante. Por ej., en la vista index, se mostrará de 1ro el campo ID, seguido de DESCRIPCION, etc.
  VISTAS = [
    { nombre: [ :index, :show ] },
    { descripcion: [ :index, :show ] },
    { estado: [ :index, :show ] },
    { creado: [ :show ] },
    { actualizado: [ :show ] },
  ]

  # --------------  CAMPOS A SER MODIFICADOS POR EL USUARIO ------------------------------
  PARAMETROS = :nombre, :descripcion, :estatus;
  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :nombre,
    presence: true,
    length: { within: 2..100 }

  validates :descripcion,
    length: { maximum: 250 }

  validates :estatus,
    inclusion: { in: ['A', 'N', 'B']
  }

  # --------------- METODOS DE LA CLASE --------------------------------------------
  # ---------------- BUSQUEDA UTILIZADA EN EL INDEX ---------------------
  def self.buscar_todos
    select('id, nombre, descripcion').
      where("estatus <> 'B'").
      order('nombre')
  end

  # --------------------------------------------------------------------
  def self.buscar_todos_activos
    select('id, nombre, descripcion').
      where("estatus = 'A'")
  end



  # ---------------- BUSQUEDA UTILIZADA EN EL SHOW ---------------------
  def self.buscar_por(id)
    select("*,
     (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.adm_usuario_id WHERE u.id = adm_maestro_tipos.usuario_creacion_id) || ' - ' || to_char(fecha_creacion, 'DD/MM/YYYY HH:MI AM') AS creado,
     (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.adm_usuario_id WHERE u.id = adm_maestro_tipos.usuario_actualizacion_id) || ' - ' || to_char(fecha_actualizacion, 'DD/MM/YYYY HH:MI AM') AS actualizado").
    where("estatus <> 'B' AND id = #{id}").
    first
  end
end
