# encoding: utf-8

class Perfil < ActiveRecord::Base
  include ModeloMetodosGenerales
  # --------------  RELACIONES CON OTRAS TABLAS --------------------------
  belongs_to  :persona, foreign_key: :persona_id
  has_one     :adm_usuario, through: :persona
  belongs_to  :adm_maestro, foreign_key: :tipo_id
  has_many    :perfil_aficionados, dependent: :destroy
  has_many    :perfil_agentes, dependent: :destroy
  has_many    :perfil_analistas, dependent: :destroy
  has_many    :perfil_arbitros, dependent: :destroy
  has_many    :perfil_directivos, dependent: :destroy
  has_many    :perfil_empresas, dependent: :destroy
  has_many    :perfil_entrenadores, dependent: :destroy
  has_many    :perfil_jugadores, dependent: :destroy
  has_many    :perfil_fisico_preparadores, dependent: :destroy
  has_many    :perfil_portero_entrenadores, dependent: :destroy
  has_many    :perfil_delegados, dependent: :destroy
  has_many    :perfil_comentarios, dependent: :destroy
  has_many    :perfil_fotos, dependent: :destroy
  has_many    :perfil_foto_comentarios, through: :perfil_fotos
  has_many    :perfil_videos, dependent: :destroy
  has_many    :perfil_video_comentarios, through: :perfil_videos
  has_many    :perfil_valoraciones, dependent: :destroy
  has_many    :perfil_logros, dependent: :destroy

  # -----  CAMPOS A SER MODIFICADOS POR EL USUARIO --------------------
  PARAMETROS = :tipo_id, :estatus;
  # --------------  TITULOS DE LOS FORMULARIOS -----------------------
  TITULOS = {
    index: 'Listado de Perfiles existentes',
    show: 'Consulta del Perfil',
    new: 'Ingresar Perfil del Perfil',
    edit: 'Editar Perfil del Perfil'
  }
  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------
  validates :tipo_id,
    presence: true

  validates :estatus,
    inclusion: { in: ['A', 'N', 'B']
  }
  #----------------------------------------------------------------------
  # --------------- METODOS DE LA CLASE ---------------------------------
  # --------- SLIDER DEL HOME ------------------
  def self.buscar_ultimos_actualizados(limite)
    sql = "-- ***** JUGADOR *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, pj.peso, pj.altura, pj.numero,
                  (SELECT e.nombre FROM equipos AS e WHERE e.id = pj.equipo_id LIMIT 1) AS nombre_equipo,
                  (SELECT c.logo FROM equipos AS e, clubes AS c WHERE e.id = pj.equipo_id AND e.club_id = c.id LIMIT 1) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_cabecero,
                  (SELECT array_agg(nombre) FROM adm_maestros WHERE estatus = 'A' AND id = ANY (pj.posicion_id)) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pj.temporada_id GROUP BY perfil_id) AS valoracion,
                  pj.fecha_creacion, CASE WHEN pj.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pj.fecha_actualizacion END,
                  (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_jugadores pj ON pj.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pj.equipo_id
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Jugador/a'
  UNION
  -- ***** DIRECTIVO *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  (SELECT c.nombre FROM clubes AS c WHERE c.id = pd.club_id LIMIT 1) AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE c.id = pd.club_id LIMIT 1) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pd.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pd.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pd.temporada_id GROUP BY perfil_id) AS valoracion,
                  pd.fecha_creacion, CASE WHEN pd.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pd.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_directivos pd ON pd.perfil_id = pf.id
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Club'
  UNION
  -- ***** AFICIONADO *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  e.nombre AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
                  pa.fecha_creacion, CASE WHEN pa.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pa.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_aficionados pa ON pa.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pa.equipo_id[1]
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Aficionado/a'
  UNION
  -- ***** ENTRENADOR *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  e.nombre AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
                  pe.fecha_creacion, CASE WHEN pe.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pe.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_entrenadores pe ON pe.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pe.equipo_id[1]
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Entrenador/a'
  UNION
  -- ***** ARBITRO *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, pa.peso, pa.altura, null,
                  (SELECT f.nombre FROM federaciones AS f WHERE f.id = pa.federacion_id LIMIT 1) AS nombre_equipo,
                  (SELECT f.logo FROM federaciones AS f WHERE f.id = pa.federacion_id LIMIT 1) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
                  pa.fecha_creacion, CASE WHEN pa.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pa.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_arbitros pa ON pa.perfil_id = pf.id
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Árbitro/a'
  UNION
  -- ***** AGENTE *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null, null, null,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
                  pa.fecha_creacion, CASE WHEN pa.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pa.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_agentes pa ON pa.perfil_id = pf.id
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Agente'
  UNION
  -- ***** EMPRESA *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, pe.nombre_comercial,
          (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel1_id) || ' / ' ||
          (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel2_id) || ' / ' ||
          (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel3_id) || ' / ' ||
          (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel4_id),
          pe.direccion, pe.telefono, pe.movil,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null , null, null, pe.correo, pe.resumen,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 LIMIT 1) AS foto_cabecero,
                  (SELECT array_agg(nombre) FROM adm_maestros m WHERE m.id = ANY(pe.categoria_id)),
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id GROUP BY perfil_id) AS valoracion,
                  pe.fecha_creacion, CASE WHEN pe.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pe.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_empresas pe ON pe.perfil_id = pf.id
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Empresa'
  UNION
  -- ***** PREPARADOR FISICO *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  e.nombre AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pp.temporada_id GROUP BY perfil_id) AS valoracion,
                  pp.fecha_creacion, CASE WHEN pp.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pp.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_fisico_preparadores pp ON pp.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pp.equipo_id[1]
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Preparador/a Físico'
  UNION
  -- ***** ENTRENADOR DE PORTEROS *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  e.nombre AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
                  pe.fecha_creacion, CASE WHEN pe.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pe.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_portero_entrenadores pe ON pe.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pe.equipo_id[1]
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Entrenador/a Porteros/as'
  UNION
  -- ***** DELEGADOS *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  e.nombre AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
                  pe.fecha_creacion, CASE WHEN pe.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pe.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_delegados pe ON pe.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pe.equipo_id[1]
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Delegado/a'
  UNION
  -- ***** ANALISTA *******
  SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
          pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
                  e.nombre AS nombre_equipo,
                  (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
                  (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
                  (null) AS posiciones,
                  (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
                  (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
                  pe.fecha_creacion, CASE WHEN pe.fecha_actualizacion IS NULL THEN '1111-11-11' ELSE pe.fecha_actualizacion END, null
  FROM perfiles pf
  INNER JOIN personas ps ON ps.id = pf.persona_id
  INNER JOIN adm_maestros m ON m.id = pf.tipo_id
  INNER JOIN perfil_analistas pe ON pe.perfil_id = pf.id
  INNER JOIN equipos e ON e.id = pe.equipo_id[1]
   WHERE pf.estatus = 'A'
      AND m.nombre = 'Analista'

  ORDER BY fecha_actualizacion DESC, fecha_creacion DESC
  LIMIT #{limite}"
    find_by_sql(sql)
  end

  #---------------------------------------------------------------------------------
  def self.buscar_jugadores_valorados(limite)
    select("perfil_jugadores.*, personas.*, perfiles.id AS perfil_id, personas.id AS persona_id, personas.foto AS foto_persona,
              (SELECT e.nombre FROM equipos AS e WHERE e.id = perfil_jugadores.equipo_id LIMIT 1) AS nombre_equipo,
              (SELECT c.logo FROM equipos AS e, clubes AS c WHERE e.id = perfil_jugadores.equipo_id AND e.club_id = c.id LIMIT 1) AS foto_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = perfiles.id AND f.tipo_id = 15 LIMIT 1) AS foto_perfil,
              (SELECT ec.nombre FROM equipos e, equipo_categorias ec WHERE ec.id = e.equipo_categoria_id AND perfil_jugadores.equipo_id = e.id) AS categoria,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM public.perfil_valoraciones pv WHERE pv.perfil_id = perfiles.id GROUP BY perfil_id) AS valoracion")
          .joins([:persona, :perfil_jugadores ])
        .where("perfiles.estatus = 'A' AND (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = perfiles.id GROUP BY perfil_id) IS NOT NULL")
        .order('valoracion DESC')
        .limit(limite)
  end
  # --------- BUSQUEDA DE UN PERFIL EN PARTICULAR ------------------
  #-----------------------------------------------------------------
  def self.buscar_por_id(id)
    sql = "-- ***** JUGADOR *******
      SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_jugadores pj ON pj.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Jugador/a'
        UNION
        -- ***** DIRECTIVO *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_directivos pd ON pd.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Club'
        UNION
        -- ***** AFICIONADO *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_aficionados pa ON pa.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Aficionado/a'
        UNION
        -- ***** ENTRENADOR *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_entrenadores pe ON pe.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Entrenador/a'
        UNION
        -- ***** ARBITRO *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_arbitros pa ON pa.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Árbitro/a'
        UNION
        -- ***** AGENTE *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_agentes pa ON pa.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Agente'
        UNION
        -- ***** EMPRESA *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_empresas pe ON pe.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
        AND m.nombre = 'Empresa'
        UNION
        -- ***** PREPARADOR FISICO *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_fisico_preparadores pp ON pp.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Preparador/a Físico'
            UNION
        -- ***** ENTRENADOR DE PORTEROS *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_portero_entrenadores pp ON pp.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Entrenador/a Porteros/as'
            UNION
        -- ***** DELEGADOS *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_delegados pp ON pp.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Delegado/a'
            UNION
        -- ***** ANALISTAS *******
          SELECT pf.id AS perfil_id, m.nombre AS tipo_perfil
        FROM perfiles pf
        INNER JOIN adm_maestros m ON m.id = pf.tipo_id
        LEFT JOIN perfil_analistas pp ON pp.perfil_id = pf.id
        WHERE pf.estatus = 'A'
        AND pf.id = #{id}
          AND m.nombre = 'Analista'"
    find_by_sql(sql)
  end
  # --- BUSQUEDA DE UN PERFIL EN PARTICULAR Y DE UN TIPO EN ESPECIFICO -
  #-----------------------------------------------------------
  def self.buscar_por_perfil_por_id(tipo_id, perfil_id)
    case tipo_id
    when 'Jugador/a'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
          concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
          concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, pj.peso, pj.altura, pj.numero,
              (SELECT e.nombre FROM equipos AS e WHERE e.id = pj.equipo_id LIMIT 1) AS nombre_equipo,
              (SELECT c.logo FROM equipos AS e, clubes AS c WHERE e.id = pj.equipo_id AND e.club_id = c.id LIMIT 1) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT array_agg(nombre) FROM adm_maestros WHERE estatus = 'A' AND id = ANY (pj.posicion_id)) AS posiciones,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pj.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pj.tipo_id) AS tipo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pj.pierna_buena_id) AS pierna_buena,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pj.temporada_id) AS temporada,
              pj.id AS perfil_detalle_id, pj.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_jugadores pj ON pj.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = pj.equipo_id
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Jugador/a'"

    when 'Club'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona,
              (SELECT c.nombre FROM clubes AS c WHERE c.id = pd.club_id LIMIT 1) AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE c.id = pd.club_id LIMIT 1) AS foto_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pd.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pd.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pd.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pd.temporada_id) AS temporada,
              pd.id AS perfil_detalle_id, pd.club_id, 0 AS equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_directivos pd ON pd.perfil_id = pf.id
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Club'"

    when 'Entrenador/a'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, e.nombre AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pe.temporada_id) AS temporada,
              pe.id AS perfil_detalle_id, pe.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_entrenadores pe ON pe.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = ANY(pe.equipo_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Entrenador/a'"

    when 'Aficionado/a'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, e.nombre AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pa.temporada_id) AS temporada,
              pa.id AS perfil_detalle_id, pa.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_aficionados pa ON pa.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = ANY(pa.equipo_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Aficionado/a'"

    when 'Árbitro/a'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, pa.peso, pa.altura,
              (SELECT f.nombre FROM federaciones AS f WHERE f.id = pa.federacion_id LIMIT 1) AS nombre_equipo,
              (SELECT f.logo FROM federaciones AS f WHERE f.id = pa.federacion_id LIMIT 1) AS foto_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pa.temporada_id) AS temporada,
              pa.id AS perfil_detalle_id, 0 AS equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_arbitros pa ON pa.perfil_id = pf.id
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Árbitro/a'"

    when 'Agente'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pa.temporada_id) AS temporada,
              pa.id AS perfil_detalle_id,
              pj.perfil_id AS perfil_jugador_id,
              (SELECT p2.primer_nombre || ' ' || p2.primer_apellido FROM personas p2, perfiles pf2 WHERE p2.id = pf2.persona_id AND pf2.id = pj.perfil_id) AS nombre_jugador,
              (SELECT f.archivo FROM perfil_fotos f, perfiles pf2 WHERE f.perfil_id = pf2.id AND pf2.id = pj.perfil_id AND f.tipo_id = 15 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_jugador,
              (SELECT e.nombre FROM equipos e WHERE e.id = pj.equipo_id) AS equipo_jugador,
              (SELECT e.id FROM equipos e WHERE e.id = pj.equipo_id) AS id_equipo_jugador, 0 AS equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_agentes pa ON pa.perfil_id = pf.id
            LEFT JOIN perfil_jugadores pj ON pj.id = ANY(pa.perfil_jugador_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Agente'"

    when 'Empresa'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              pe.nombre_comercial AS nombre_persona_largo,
              (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel1_id) || ' / ' ||
              (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel2_id) || ' / ' ||
              (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel3_id) || ' / ' ||
              (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel4_id) AS localizacion,
              pe.direccion, pe.telefono, pe.movil, pe.correo, pe.resumen,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 LIMIT 1) AS foto_cabecero,
              (SELECT array_agg(nombre) FROM adm_maestros m WHERE m.id = ANY(pe.categoria_id)) AS categorias,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id GROUP BY perfil_id) AS valoracion,
              pe.id AS perfil_detalle_id, 0 AS equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_empresas pe ON pe.perfil_id = pf.id
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Empresa'"

    when 'Preparador/a Físico'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, e.nombre AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pp.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pp.temporada_id) AS temporada,
              pp.id AS perfil_detalle_id, pp.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_fisico_preparadores pp ON pp.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = ANY(pp.equipo_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Preparador/a Físico'"

    when 'Entrenador/a Porteros/as'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, e.nombre AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pp.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pp.temporada_id) AS temporada,
              pp.id AS perfil_detalle_id, pp.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_portero_entrenadores pp ON pp.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = ANY(pp.equipo_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Entrenador/a Porteros/as'"

    when 'Delegado/a'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, e.nombre AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pp.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pp.temporada_id) AS temporada,
              pp.id AS perfil_detalle_id, pp.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_delegados pp ON pp.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = ANY(pp.equipo_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Delegado/a'"

    when 'Analista'
      sql = "SELECT m.nombre AS tipo_perfil, date_part('year',age(ps.fecha_nacimiento)) AS edad,
              concat(ps.primer_nombre, ' ',ps.segundo_nombre, ' ', ps.primer_apellido, ' ',  ps.segundo_apellido) AS nombre_persona_largo,
              concat(ps.primer_nombre, ' ', ps.primer_apellido) AS nombre_persona_corto, ps.apodo,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.sexo_id) AS sexo, ps.telefono,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = ps.nivel_estudios_id) AS nivel_estudios,
              pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, e.nombre AS nombre_equipo,
              (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
              (SELECT ce.nombre FROM equipo_categorias ce WHERE ce.id = e.equipo_categoria_id) AS categoria_equipo,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_perfil,
              (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pp.temporada_id LIMIT 1) AS foto_cabecero,
              (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
              (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pp.temporada_id GROUP BY perfil_id) AS valoracion,
              (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pp.temporada_id) AS temporada,
              pp.id AS perfil_detalle_id, pp.equipo_id, pf.tipo_id,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel1_id) AS territorio_nivel1,
              (SELECT t.nombre FROM adm_territorios AS t WHERE t.id = ps.territorio_nivel2_id) AS territorio_nivel2
            FROM perfiles pf
            INNER JOIN personas ps ON ps.id = pf.persona_id
            INNER JOIN adm_maestros m ON m.id = pf.tipo_id
            LEFT JOIN perfil_analistas pp ON pp.perfil_id = pf.id
            LEFT JOIN equipos e ON e.id = ANY(pp.equipo_id)
            WHERE pf.estatus = 'A'
            AND pf.id = #{perfil_id}
              AND m.nombre = 'Analista'"
    end
    find_by_sql(sql)
  end

  # --------- BUSQUEDA DEL ULTIMO PERFIL POR EL ID DE LA PERSONA ------------------
  def self.buscar_por_id_persona(id_persona)
    sql = "-- ***** JUGADOR *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, pj.peso, pj.altura, pj.numero,
        (SELECT e.nombre FROM equipos AS e WHERE e.id = pj.equipo_id LIMIT 1) AS nombre_equipo,
        (SELECT c.logo FROM equipos AS e, clubes AS c WHERE e.id = pj.equipo_id AND e.club_id = c.id LIMIT 1) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pj.temporada_id LIMIT 1) AS foto_cabecero,
        (SELECT array_agg(nombre) FROM adm_maestros WHERE estatus = 'A' AND id = ANY (pj.posicion_id)) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pj.temporada_id GROUP BY perfil_id) AS valoracion,
        (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pj.tipo_id) AS tipo,
        (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pj.pierna_buena_id) AS pierna_buena,
        (SELECT nombre FROM adm_maestros WHERE estatus = 'A' AND id = pj.temporada_id) AS temporada,
        pj.id AS perfil_detalle_id, pj.equipo_id
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_jugadores pj ON pj.perfil_id = pf.id
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Jugador/a'

      UNION
      -- ***** DIRECTIVO *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        (SELECT c.nombre FROM clubes AS c WHERE c.id = pd.club_id LIMIT 1) AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE c.id = pd.club_id LIMIT 1) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pd.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pd.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pd.temporada_id GROUP BY perfil_id) AS valoracion,
        null, null, null, pd.id AS perfil_detalle_id, 0
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_directivos pd ON pd.perfil_id = pf.id
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Club'

      UNION
      -- ***** AFICIONADO *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        e.nombre AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pa.id AS perfil_detalle_id, pa.equipo_id[2]
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_aficionados pa ON pa.perfil_id = pf.id
      LEFT JOIN equipos e ON e.id = pa.equipo_id[2]
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Aficionado/a'

      UNION
      -- ***** ENTRENADOR *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        e.nombre AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pe.id AS perfil_detalle_id, pe.equipo_id[2]
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_entrenadores pe ON pe.perfil_id = pf.id
      LEFT JOIN equipos e ON e.id = pe.equipo_id[2]
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Entrenador/a'

      UNION
      -- ***** ARBITRO *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, pa.peso, pa.altura, null,
        (SELECT f.nombre FROM federaciones AS f WHERE f.id = pa.federacion_id LIMIT 1) AS nombre_equipo,
        (SELECT f.logo FROM federaciones AS f WHERE f.id = pa.federacion_id LIMIT 1) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pa.id AS perfil_detalle_id, 0
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_arbitros pa ON pa.perfil_id = pf.id
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Árbitro/a'

      UNION
      -- ***** AGENTE *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null, null, null,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pa.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pa.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pa.id AS perfil_detalle_id, 0
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_agentes pa ON pa.perfil_id = pf.id
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Agente'

      UNION
      -- ***** EMPRESA *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, pe.nombre_comercial,
        (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel1_id) || ' / ' ||
        (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel2_id) || ' / ' ||
        (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel3_id) || ' / ' ||
        (SELECT nombre FROM adm_territorios t WHERE t.id = pe.territorio_nivel4_id),
        pe.direccion, pe.telefono, pe.movil,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null , null, null, pe.correo, pe.resumen,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 LIMIT 1) AS foto_cabecero,
        (SELECT array_agg(nombre) FROM adm_maestros m WHERE m.id = ANY(pe.categoria_id)),
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id GROUP BY perfil_id) AS valoracion,
        null,null,null, pe.id AS perfil_detalle_id, 0
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_empresas pe ON pe.perfil_id = pf.id
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Empresa'

      UNION
      -- ***** PREPARADOR FISICO *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        e.nombre AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pe.id AS perfil_detalle_id, pe.equipo_id[2]
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_fisico_preparadores pe ON pe.perfil_id = pf.id
      LEFT JOIN equipos e ON e.id = pe.equipo_id[2]
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Preparador/a Físico'

          UNION
      -- ***** ENTRENADOR DE PORTEROS *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        e.nombre AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pe.id AS perfil_detalle_id, pe.equipo_id[2]
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_portero_entrenadores pe ON pe.perfil_id = pf.id
      LEFT JOIN equipos e ON e.id = pe.equipo_id[2]
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Entrenador/a Porteros/as'

          UNION
      -- ***** DELEGADOS *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        e.nombre AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pe.id AS perfil_detalle_id, pe.equipo_id[2]
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_delegados pe ON pe.perfil_id = pf.id
      LEFT JOIN equipos e ON e.id = pe.equipo_id[2]
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Delegado/a'

          UNION
      -- ***** ANALISTA *******
        SELECT m.nombre AS tipo_perfil, ps.fecha_nacimiento, ps.primer_nombre, ps.segundo_nombre, ps.primer_apellido, ps.segundo_apellido, ps.apodo,
        pf.id AS perfil_id, ps.id AS persona_id, ps.foto AS foto_persona, null, null, null,
        e.nombre AS nombre_equipo,
        (SELECT c.logo FROM clubes AS c WHERE e.club_id = c.id) AS foto_equipo,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 15 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_perfil,
        (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = pf.id AND f.tipo_id = 16 AND f.temporada_id = pe.temporada_id LIMIT 1) AS foto_cabecero,
        (null) AS posiciones,
        (SELECT t.gentilicio FROM adm_territorios AS t WHERE t.id = ps.nacionalidad_id) AS nacionalidad,
        (SELECT CAST(sum(valoracion) AS float) / CAST(count(id) AS float) FROM perfil_valoraciones pv WHERE pv.perfil_id = pf.id AND pv.temporada_id = pe.temporada_id GROUP BY perfil_id) AS valoracion,
        null,null,null, pe.id AS perfil_detalle_id, pe.equipo_id[2]
      FROM perfiles pf
      INNER JOIN personas ps ON ps.id = pf.persona_id
      INNER JOIN adm_maestros m ON m.id = pf.tipo_id
      LEFT JOIN perfil_analistas pe ON pe.perfil_id = pf.id
      LEFT JOIN equipos e ON e.id = pe.equipo_id[2]
      WHERE pf.estatus = 'A'
      AND pf.persona_id = #{id_persona}
        AND m.nombre = 'Analista'"
    find_by_sql(sql).last
  end

  #---------------------------------------------------------------------------------
  def self.buscar_amigos_por_equipo(perfil)
    if perfil.equipo_id.nil?
      id_equipo = 'null'
    else
      id_equipo = perfil.equipo_id.to_s.gsub('[', '{').gsub(']', '}').gsub('nil', 'null')
    end

    if perfil.tipo_perfil == 'Entrenador/a' || perfil.tipo_perfil == 'Aficionado/a' || perfil.tipo_perfil == 'Preparador/a Físico' || perfil.tipo_perfil == 'Entrenador/a Porteros/as' || perfil.tipo_perfil == 'Delegado/a' || perfil.tipo_perfil == 'Analista'
      id_equipo = '{null}' if id_equipo == 'null'
      filtro_equipos = " pj.equipo_id = ANY('#{id_equipo}') OR pe.equipo_id && '#{id_equipo}' OR pa.equipo_id && '#{id_equipo}' OR pp.equipo_id && '#{id_equipo}' OR ppe.equipo_id && '#{id_equipo}' OR pd.equipo_id && '#{id_equipo}' OR pana.equipo_id && '#{id_equipo}'"
    else
      filtro_equipos = "pj.equipo_id = #{id_equipo} OR #{id_equipo} = ANY(pe.equipo_id) OR #{id_equipo} = ANY(pa.equipo_id) OR #{id_equipo} = ANY(pp.equipo_id) OR #{id_equipo} = ANY(ppe.equipo_id) OR #{id_equipo} = ANY(pd.equipo_id) OR #{id_equipo} = ANY(pana.equipo_id)"
    end
    sql = "SELECT p.*, personas.*, p.id AS perfil_id, personas.id AS persona_id, personas.foto AS foto_persona,
                    (SELECT f.archivo FROM perfil_fotos AS f WHERE f.perfil_id = p.id AND f.tipo_id = 15 LIMIT 1) AS foto_perfil,
                    (SELECT array_agg(nombre) FROM adm_maestros WHERE estatus = 'A' AND id = ANY (pj.posicion_id)) AS posiciones
                FROM perfiles p
                INNER JOIN personas ON personas.id = p.persona_id
                LEFT JOIN perfil_jugadores pj ON p.id = pj.perfil_id
                LEFT JOIN perfil_entrenadores pe ON p.id = pe.perfil_id
                LEFT JOIN perfil_aficionados pa ON p.id = pa.perfil_id
                LEFT JOIN perfil_fisico_preparadores pp ON p.id = pp.perfil_id
                LEFT JOIN perfil_portero_entrenadores ppe ON p.id = ppe.perfil_id
                LEFT JOIN perfil_delegados pd ON p.id = pd.perfil_id
                LEFT JOIN perfil_analistas pana ON p.id = pana.perfil_id
                WHERE p.id <> #{perfil.perfil_id}
                    AND (#{filtro_equipos})
                ORDER BY p.fecha_creacion DESC"
    find_by_sql(sql)
  end
  #---------------------------------------------------------------------------------
  def self.buscar_rivales_de_categoria_por_persona(persona_id, tipo_perfil)
    if tipo_perfil == 'Entrenador/a' || tipo_perfil == 'Aficionado/a' || tipo_perfil == 'Preparador/a Físico' || tipo_perfil == 'Entrenador/a Porteros/as' || tipo_perfil == 'Delegado/a' || tipo_perfil == 'Analista'
      filtro_equipos1 = 'e.id = pj.equipo_id OR e.id = ANY(pe.equipo_id) OR e.id = ANY(pa.equipo_id)'
      filtro_equipos2 = "LEFT JOIN perfil_jugadores pj ON e.id = pj.equipo_id
                          LEFT JOIN perfil_entrenadores pe ON e.id = ANY(pe.equipo_id)
                          LEFT JOIN perfil_aficionados pa ON e.id = ANY(pa.equipo_id)
                          LEFT JOIN perfil_fisico_preparadores pp ON e.id = ANY(pp.equipo_id)
                          LEFT JOIN perfil_portero_entrenadores ppe ON e.id = ANY(ppe.equipo_id)
                          LEFT JOIN perfil_delegados pd ON e.id = ANY(pd.equipo_id)
                          LEFT JOIN perfil_analistas pana ON e.id = ANY(pana.equipo_id)"

    else
      filtro_equipos1 = 'e.id = pj.equipo_id OR e.id = ANY(pe.equipo_id) OR e.id = ANY(pa.equipo_id)'
      filtro_equipos2 = "LEFT JOIN perfil_jugadores pj ON e.id = pj.equipo_id
                          LEFT JOIN perfil_entrenadores pe ON e.id = ANY(pe.equipo_id)
                          LEFT JOIN perfil_aficionados pa ON e.id = ANY(pa.equipo_id)
                          LEFT JOIN perfil_fisico_preparadores pp ON e.id = ANY(pp.equipo_id)
                          LEFT JOIN perfil_portero_entrenadores ppe ON e.id = ANY(ppe.equipo_id)
                          LEFT JOIN perfil_delegados pd ON e.id = ANY(pd.equipo_id)
                          LEFT JOIN perfil_analistas pana ON e.id = ANY(pana.equipo_id)"
    end

    sql = "SELECT DISTINCT p.persona_id, personas.primer_nombre || ' ' || personas.primer_apellido AS nombre_persona, e.id,
                    e.nombre AS nombre_equipo, personas.foto AS foto_persona,
                    (SELECT f.archivo FROM perfil_fotos f WHERE f.perfil_id = p.id AND f.tipo_id = 15 LIMIT 1) AS foto_perfil,
                    (SELECT array_agg(nombre) FROM adm_maestros WHERE estatus = 'A' AND id = ANY (pj.posicion_id)) AS posiciones,
                    (SELECT ec.nombre FROM equipo_categorias ec WHERE ec.id = e.equipo_categoria_id LIMIT 1) AS equipo_categoria
                FROM equipos e, perfiles p
                    INNER JOIN personas ON personas.id = p.persona_id
                    LEFT JOIN perfil_jugadores pj ON p.id = pj.perfil_id
                    LEFT JOIN perfil_entrenadores pe ON p.id = pe.perfil_id
                    LEFT JOIN perfil_fisico_preparadores pp ON p.id = pp.perfil_id
                    LEFT JOIN perfil_portero_entrenadores ppe ON p.id = ppe.perfil_id
                    LEFT JOIN perfil_delegados pd ON p.id = pd.perfil_id
                    LEFT JOIN perfil_analistas pana ON p.id = pana.perfil_id
                    LEFT JOIN perfil_aficionados pa ON p.id = pa.perfil_id
                WHERE (#{filtro_equipos1})
        AND e.equipo_categoria_id = ANY (SELECT e.equipo_categoria_id
                                                        FROM perfiles p, equipos e
                                                            #{filtro_equipos2}
                                                        WHERE p.persona_id = #{persona_id}
                                                            AND (pj.perfil_id = p.id OR pe.perfil_id = p.id OR pa.perfil_id = p.id)
                                                        ORDER BY p.fecha_creacion DESC)
            AND NOT e.id = ANY(SELECT e.id
                            FROM perfiles p, equipos e
                                #{filtro_equipos2}
                            WHERE p.persona_id = #{persona_id}
                                AND (pj.perfil_id = p.id OR pe.perfil_id = p.id OR pa.perfil_id = p.id)
                            ORDER BY p.fecha_creacion DESC)"
    find_by_sql(sql)
  end
end
