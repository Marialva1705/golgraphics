# encoding: utf-8

class AdmUsuario < ActiveRecord::Base
  self.table_name = 'adm_usuarios'

  before_destroy :eliminar_conversaciones, prepend: true

  # --------------  MODULOS DE LA GEMA ------------------------------------------------------
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #devise :database_authenticatable, :registerable,
         #:recoverable, :rememberable, :trackable, :validatable
  #devise :omniauthable, omniauth_providers: [:twitter, :facebook, :google_oauth2, :linkedin]

  public

    def self.find_or_create_by_omniauth(auth)
      if AdmUsuario.where(email: auth.info.email).exists?
        usuario = AdmUsuario.find_by(email: auth.info.email)
        usuario.adm_usuario_identidades.find_or_create_by(proveedor: auth.provider, uid: auth.uid)
      elsif AdmUsuarioIdentidad.find_by_auth(auth.provider,auth.uid).exists?
        usuario = AdmUsuarioIdentidad.find_by_auth(auth.provider,auth.uid).first.adm_usuario
      else
        usuario = AdmUsuario.create(
            email: auth.info.email,
            password: Devise.friendly_token[0,20],
            adm_rol_id: 4,
            estatus: 'A',
            token_activacion: (Digest::MD5.hexdigest "#{SecureRandom.hex(10)}-#{DateTime.now.to_s}")
          )

        usuario.adm_usuario_identidades.create(proveedor: auth.provider,uid: auth.uid)
      end

      usuario
    end

  # --------------  RELACIONES CON OTRAS TABLAS ---------------------------------------------
  #acts_as_voter
  has_many :partido_videos, dependent: :destroy
  has_many :partido_video_comentarios, dependent: :destroy
  belongs_to :adm_rol, foreign_key: :adm_rol_id # modelo - en singular
  has_one :persona, dependent: :destroy
  has_many :perfiles, through: :persona
  has_many :adm_usuario_identidades, dependent: :destroy

  # --------------  TITULOS DE LOS FORMULARIOS ---------------------------------------------
  TITULOS = {
    index: 'Listado de Usuarios existentes',
    show: 'Consulta de Usuario existente',
    new: 'Ingresar Usuario nuevo',
    edit: 'Editar Usuario existente'
  }

  # --------------  CAMPOS A MOSTRAR EN LOS FORMULARIOS INDEX Y SHOW -----------------------
  # Especifique lo atributos que desee aparezcan en las vistas.
  # El orden es importante. Por ej., en la vista index, se mostrará de 1ro el campo ID, seguido de DESCRIPCION, etc.
  VISTAS = [
    { id:         [ :index, :show ] },
    { email:     [ :index, :show ] },
    { email: [ :index, :show ] },
    { adm_rol_id: [ :index, :show ] },
    { estado:     [ :index, :show ] },
#    { creado:     [ :index, :show ] },
#    { actualizado: [ :index, :show ] },
  ]

  # --------------  CAMPOS A SER MODIFICADOS POR EL USUARIO ------------------------------
  PARAMETROS = :email, :email_confirmation, :password, :password_confirmation, :adm_rol_id, :token_activacion, :estatus;

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  # validates :nombre, #:nombre_confirmation,
  #   presence: { message: ' es requerido' },
  #   length: { within: 4..99 },
  #   format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
  #   message: '* Lo que ha escrito no es un correo' }

  validates :email, confirmation: true

  # validates :nombre,
  #   uniqueness: { case_sensitive: false ,
  #   message: 'ya esta registrado' },
  #   confirmation: true

#  validates :password,
#    presence: true,
#    length: { within: 4..50 }

  validates :estatus,
    inclusion: { in: ['A', 'N', 'B']
  }

  # --------------- METODOS DE LA CLASE --------------------------------------------
  #---------------------------------------------------------------------------------

  #---------------------------------------------------------------------------------
  def self.buscar_por(id_usuario)
    select("*,
                  (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.adm_usuario_id WHERE u.id = adm_usuarios.usuario_creacion_id) || ' - ' || to_char(fecha_creacion, 'DD/MM/YYYY HH:MI AM') AS creado,
                  (SELECT p.primer_nombre || ' ' || p.primer_apellido || ' (' || u.email || ')' FROM adm_usuarios AS u LEFT JOIN personas AS p ON u.id = p.adm_usuario_id WHERE u.id = adm_usuarios.usuario_actualizacion_id) || ' - ' || to_char(fecha_actualizacion, 'DD/MM/YYYY HH:MI AM') AS actualizado,
                  (SELECT nombre FROM adm_roles WHERE estatus = 'A' AND id = adm_usuarios.adm_rol_id) AS rol")
    .where("estatus <> 'B' AND id = #{id_usuario}")
  end

  #---------------------------------------------------------------------------------
  def self.buscar_usuario_clave(usuario, clave)
    select('id, nombre').
      where("estatus='A' AND clave = '#{clave}' AND nombre = '#{usuario}'")
  end

  #---------------------------------------------------------------------------------
  def self.buscar_todos_por_nivel(nivel, usuario_id)
    if nivel < 3
      find_by_sql("SELECT id, email, estatus, adm_rol_id,
                         (SELECT nombre FROM adm_roles WHERE estatus = 'A' AND id = adm_usuarios.adm_rol_id) AS rol
                  FROM adm_usuarios
                    WHERE estatus <> 'B'")
    else
      find_by_sql("SELECT id, email, estatus, adm_rol_id,
                         (SELECT nombre FROM adm_roles WHERE estatus = 'A' AND id = adm_usuarios.adm_rol_id) AS rol
                  FROM adm_usuarios
                  WHERE (estatus <> 'B' AND id = #{usuario_id})
                     OR (estatus <> 'B' AND usuario_creacion_id = #{usuario_id})")
    end
  end
  
  #---------------------------------------------------------------------------------
  def self.buscar_por_usuario(usuario)
    select('id, email').
      where("estatus = 'A' AND email = '#{usuario}'").
      first
  end
  
  #---------------------------------------------------------------------------------
  def self.buscar_por_rol(rol)
    select("personas.id, personas.primer_nombre || ' ' || personas.primer_apellido || ' ' || personas.segundo_apellido AS nombre")
    .joins([:persona, :adm_rol])
    .where("adm_roles.nombre = '#{rol}' AND personas.estatus = 'A'")
  end
  
  #---------------------------------------------------------------------------------
#  def self.from_omniauth(auth)
#    where(id: $session_devise_user_id ).first_or_initialize.tap do |user|
#      #user.nombre           = auth.info.name
#      #user.password_digest  = 'bnhjui89'
#      #user.adm_rol_id       =  1
#      puts '1111--------------------------------------------------', user.inspect
#      user.token_activacion = auth.credentials.token
#      user.refresh_token    = auth.credentials.refresh_token
#      puts '2222--------------------------------------------------', user.inspect
#      
#      user.save!
#    end
#  end

  def self.from_omniauth(auth)
    record = where(id: $session_devise_user_id)
    record.update(token_activacion: auth.credentials.token, refresh_token: auth.credentials.refresh_token )
  end
    
  #---------------------------------------------------------------------------------
  def self.current
    Thread.current[:adm_usuario]
  end
    
  #---------------------------------------------------------------------------------
  def self.current=(user)
    Thread.current[:adm_usuario] = user
  end
  
  #---------------------------------------------------------------------------------
  def self.buscar_todos
    select("adm_usuarios.id AS u_id, adm_usuarios.email AS u_nombre, adm_usuarios.estatus AS u_estatus, adm_roles.nombre AS r_nombre, 
            personas.id AS p_id, personas.primer_nombre || ' ' || personas.primer_apellido AS p_nombre, 
            perfiles.id AS f_id, 
           (SELECT nombre FROM adm_maestros WHERE id = perfiles.tipo_id) AS f_tipo")
    .left_joins([:persona, :perfiles, :adm_rol])
    .where("adm_usuarios.estatus <> 'B'")
  end

  def usuarios_rol
    AdmUsuario.joins(:adm_rol).group("adm_roles.id").pluck("adm_roles.nombre, count(*) as total")
  end
  def mostrar_usuario_nacionalidad
       sql = "SELECT COUNT(*) AS total,'Doble nacionalidad' as nacionalidad FROM personas p INNER JOIN adm_usuarios au ON p.adm_usuario_id=au.id 
        WHERE au.estatus = 'A' AND p.nacionalidad_id is not null AND p.nacionalidad2_id IS NOT NULL
        UNION 
        SELECT COUNT(*) AS total, 'Una nacionalidad' as nacionalidad 
        FROM personas p INNER JOIN adm_usuarios au ON p.adm_usuario_id=au.id 
        WHERE au.estatus = 'A' AND p.nacionalidad_id is not null AND p.nacionalidad2_id is null
        UNION 
        SELECT COUNT(*) AS total, 'Datos incompletos' as nacionalidad 
        FROM public.adm_usuarios au LEFT JOIN personas p ON p.adm_usuario_id=au.id WHERE p.id is null
        ORDER BY nacionalidad"
       AdmUsuario.find_by_sql(sql)
  end

  def mostrar_usuario_por_nacionalidad
       AdmUsuario.joins(:persona).group("personas.nacionalidad_id").pluck("personas.nacionalidad_id, count(*) as total")
  end
  private

    def eliminar_conversaciones
      Conversacion.de(self.id).destroy_all
    end
  
end
