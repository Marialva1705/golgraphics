class PerfilValoracione < ApplicationRecord
	 self.table_name = 'perfil_valoraciones'
	 belongs_to :persona, :foreign_key => :persona_id	
	 belongs_to :valoracion_categoria, :foreign_key => :valoracion_categoria_id
	 

	 def ubicacion_jugadores
	 	PerfilValoracione.joins(:valoracion_categoria,:persona).where({ personas: { estatus: "A" }, valoracion_categorias: { estatus: "A" } }).order(valoracion: :desc).last(5)	 	
	 	#ValoracionCategoria.joins :perfil_valoraciones	 	
	 end
end