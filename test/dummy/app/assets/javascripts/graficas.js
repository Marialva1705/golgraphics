//= require rails-ujs
//= require turbolinks

//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets

//= require bootstrap-sprockets

//= require select2
//= require select2_locale_es

//= require lightbox-bootstrap

//= require moment
//= require bootstrap-datetimepicker
//= require moment/es.js

//= require bootstrap-datepicker
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.es.js

//= require cookies_eu
//= require highcharts



// ****************************************************************************************************
// ****************************************************************************************************
//$(document).ready(function () {
$( document ).on('turbolinks:load', function() {
    $("#ciudade_graficas").css("display","none");
    $("#parroquias_graficas").css("display","none");
    $("#parroquias_municipios").css("display","none");

    // ------------------ GRAFICA DE CIUDADES -----------------------------------
    $('#paises_Usuarios').on('change', function() {
     id = $('#paises_Usuarios').val();
     $("#titulo_cidades").empty();
     $("#ciudades").empty();
     $("#graficaCiudades").empty();
     $("#titulo_cidades").text("Usuarios de " + $(this).find('option:selected').attr('id'));
     $("#ciudades").append("<option value='0'>Seleccione</option>");
     $.ajax({
          type : 'get',
          url : "/grafica1",
          data : "id=" + id,
          dataType : 'json',
          async : false,
          success: function(result) {
            var dataCiudades = new Array(); 
            $.each(result,function(indice,valor){               
                dataCiudades.push({"name":valor["nombre"],"y":valor["total"],"drilldown":valor["nombre"]});
                $("#ciudades").append("<option value="+valor["territorio_nivel2_id"]+" id="+valor["nombre"]+">"+valor["nombre"]+"</option>");
            })
            Highcharts.chart('graficaCiudades', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category'
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: dataCiudades
                }],
            });
        }
      });
    $("#ciudade_graficas").css("display","block");
    })
   // ------------------ GRAFICA DE PARROQUIAS -----------------------------------
    $('#ciudades').on('change', function() {
     idCiudad = $('#ciudades').val();
     idPais = $('#paises_Usuarios option:selected').val();
     $("#titulo_parroquias").empty();
     $("#parroquias").empty();
     $("#graficaParroquias").empty();
     $("#titulo_parroquias").text("Usuarios de " + $(this).find('option:selected').attr('id'));
     $("#parroquias").append("<option value='0'>Seleccione</option>");
     $.ajax({
          type : 'get',
          url : "/grafica2",
          data : "idCiudad=" + idCiudad + "&idPais="+ idPais ,
          dataType : 'json',
          async : false,
          success: function(result) {
            var dataParroquias = new Array(); 
            $.each(result,function(indice,valor){               
                dataParroquias.push({"name":valor["nombre"],"y":valor["total"],"drilldown":valor["nombre"]});
                $("#parroquias").append("<option value="+valor["territorio_nivel3_id"]+" id="+valor["nombre"]+">"+valor["nombre"]+"</option>");
            })
            Highcharts.chart('graficaParroquias', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category'
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: dataParroquias
                }],
            });
        }
      });
    $("#parroquias_graficas").css("display","block");
    })
  // ------------------ GRAFICA DE MUNICIPIOS -----------------------------------
    $('#parroquias').on('change', function() {
     idCiudad = $('#ciudades option:selected').val();
     idPais = $('#paises_Usuarios option:selected').val();
     idParroquia = $('#parroquias').val();
     $("#titulo_municipios").empty();
     $("#graficaMunicipios").empty();
     $("#titulo_municipios").text("Usuarios de " + $(this).find('option:selected').attr('id'));
     $.ajax({
          type : 'get',
          url : "/grafica3",
          data : "idCiudad=" + idCiudad + "&idPais="+ idPais + "&idParroquia="+ idParroquia ,
          dataType : 'json',
          async : false,
          success: function(result) {
            var dataParroquias = new Array(); 
            $.each(result,function(indice,valor){               
                dataParroquias.push({"name":valor["nombre"],"y":valor["total"],"drilldown":valor["nombre"]});
            })
            Highcharts.chart('graficaMunicipios', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category'
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: dataParroquias
                }],
            });
        }
      });
    $("#parroquias_municipios").css("display","block");
    })
});
