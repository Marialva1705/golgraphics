# encoding: utf-8

class InicioController < ApplicationController
    #-------------------------------------------------------------------
    def home
        @inf_equipo = ValoracionCategoria.all       
        @usuarios = AdmUsuario.new()
        @usaurios_rol = @usuarios.usuarios_rol
        @maestros = AdmMaestro.new()
        @maestros_tipos = @maestros.buscar_maestro_tipos
        @usuario_nacionalidad = @usuarios.mostrar_usuario_nacionalidad
        @usuario_por_nacionalidad = @usuarios.mostrar_usuario_por_nacionalidad
        @personas = Persona.new()
        @usuarios_por_paises = @personas.mostrar_usuarios_por_paises
        @usuarios_por_paises_id = @personas.mostrar_usuarios_por_paises_id


        # @inf_equipo = ValoracionCategoria.all
       

    end
    
    def show_partial
        @personas = Persona.new()
        @personas_ciudad = @personas.mostrar_usuarios_por_ciudad(params[:id])
        respond_to do |format|
            format.js {render :json => @personas_ciudad}
        end
    end

    def show_graficaParroquias
        @personas = Persona.new()
        @personas_ciudad = @personas.mostrar_usuarios_por_parroquia(params[:idPais],params[:idCiudad])
        respond_to do |format|
            format.js {render :json => @personas_ciudad}
        end
    end

    def show_graficaMunicipios
        @personas = Persona.new()
        @personas_ciudad = @personas.mostrar_usuarios_por_municipio(params[:idPais],params[:idCiudad],params[:idParroquia])
        respond_to do |format|
            format.js {render :json => @personas_ciudad}
        end
    end



end

